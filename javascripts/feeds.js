google.load("feeds", "1");

angular.module('FeedApp', [])
  .controller('FeedController', ['$scope', function($scope) {
    var feeds = ["http://feeds.feedburner.com/BranchAndBound", "http://jagodevreede.blogspot.com/feeds/posts/default", "http://www.planetmarrs.net/feed/", "http://paulonjava.blogspot.com/feeds/posts/default", "http://paulbakker.io/feed.xml"];
    
    $scope.posts = [];

    _.each(feeds, function(feed) {
      var feedLoader = new google.feeds.Feed(feed);
      feedLoader.load(updatePage);  
    });
    

    function updatePage(result) {
      var posts = $scope.posts;

      posts.push(_.filter(result.feed.entries, function(entry) {
        var categories = _.map(entry.categories, function(category) {
            return category.toLowerCase();
        })

        var title = entry.title.toLowerCase();
        if(_.contains(categories, "osgi") || _.contains(categories, "amdatu") || title.indexOf("osgi") != -1 || title.indexOf("amdatu") != -1) {
          return true;
        }
      }));
      
      posts = _.sortBy(_.flatten(posts), function(post) {
          return Date.parse(post.publishedDate);
      }).reverse();

      $scope.posts = posts;

      $scope.$digest();

    }

}]);